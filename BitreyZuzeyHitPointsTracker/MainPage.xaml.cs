﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Marketplace;
using Microsoft.Phone.Tasks;
using BitreyZuzey.Phone.Controls;
using System.IO;
using System.IO.IsolatedStorage;


namespace BitreyZuzeyHitPointsTracker
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private void btnAddChar_Click(object sender, EventArgs e)
        {
            LicenseInformation lic = new LicenseInformation();
            CharViewModel cvm = (CharViewModel)App.Current.Resources["CharViewModelDataSource"];
            if (lic.IsTrial() == true && cvm.Chars.Count >= 1)
            {
                NotificationBox.Show(
                    "Trial Version",
                    "The trial version supports only one character. Would you like to go to the marketplace?",
                    new NotificationBoxCommand("Yes", () =>
                    {
                        MarketplaceReviewTask marketplaceReviewTask = new MarketplaceReviewTask();
                        marketplaceReviewTask.Show();
                    }),
                    new NotificationBoxCommand("No", () => { }));
                return;
            }
            
            //Not a trial version. check if quota of chars is reached.
            if (cvm.Chars.Count >= 10)
            {
                NotificationBox.Show(
                    "Max Characters",
                    "Sorry. You have reached the maximum number of characters supported",
                    new NotificationBoxCommand("Okay", () => { }));
                return;
            }

            //Now we can add the char
            NotificationBoxWithInput notification = null;
            NotificationBoxWithInput.ShowWithTextInput(
                "Character Name",
                "What is your character's name?",
                "",
                GetType().ToString(),
                out notification,
                new NotificationBoxCommand("Okay", () => { AddChar(notification.TextInputOption); }),
                new NotificationBoxCommand("Cancel", () => { }));
        }

        private void btnDeleteChar_Click(object sender, EventArgs e)
        {
            if (pivot.Items.Count == 0)
            {
                return;
            }

            int indexToDelete = pivot.SelectedIndex;
            NotificationBox.Show(
                "Delete Character",
                "Are you sure?",
                new NotificationBoxCommand("Yes", () => { if (pivot.Items.Count > 0) { DeleteChar(indexToDelete); } }),
                new NotificationBoxCommand("No", () => { }));
        }

        private void btnResetChar_Click(object sender, EventArgs e)
        {
            if (pivot.Items.Count == 0)
            {
                return;
            }
            int indexToReset = pivot.SelectedIndex;
            CharViewModel cvm = (CharViewModel)App.Current.Resources["CharViewModelDataSource"];

            NotificationBox.Show(
                "Reset Character",
                "This will reset the current character. Are you sure?",
                new NotificationBoxCommand("Yes", () => { cvm.Chars[indexToReset].ResetChar(); }),
                new NotificationBoxCommand("No", () => { }));
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            NotificationBox.Show(
                "About",
                "Developed by Bitrey Zuzey.",
                new NotificationBoxCommand("Store", () =>
                {
                    MarketplaceReviewTask marketplaceReviewTask = new MarketplaceReviewTask();
                    marketplaceReviewTask.Show();
                }),
                new NotificationBoxCommand("Email", () =>
                {
                    EmailComposeTask email = new EmailComposeTask();
                    email.Subject = "D&D HP Tracker";
                    email.To = "bitreyzuzey@gmail.com";
                    email.Show();
                }),
                new NotificationBoxCommand("Thanks", () => { }));
        }

        private void AddChar(string name)
        {
            name = name.Trim();
            CharViewModel cvm = (CharViewModel)App.Current.Resources["CharViewModelDataSource"]; 
            if (IsValidName(name))
            {
                var newChar = new CharViewModel.Char() { Name = name };
                cvm.AddChar(newChar);
                pivot.SelectedItem = pivot.Items[pivot.Items.Count - 1];
            }
        }

        private void DeleteChar(int index)
        {
            CharViewModel cvm = (CharViewModel)App.Current.Resources["CharViewModelDataSource"];

            // deleting the only element
            if (pivot.Items.Count == 1)
            {
                cvm.DeleteChar(index);
                pivot.SelectedIndex = -1;
                return;
            }

            // deleting the last element
            if (index == pivot.Items.Count - 1)
            {
                cvm.DeleteChar(index);
                pivot.SelectedItem = pivot.Items[pivot.Items.Count - 1];
                return;
            }

            // deleting in the middle
            cvm.DeleteChar(index);
            pivot.SelectedItem = pivot.Items[index];
        }

        private bool IsValidName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return false;
            }

            if (name.Contains(";"))
            {
                return false;
            }

            return true;
        }

        private void btnIncCurrHp_Click(object sender, RoutedEventArgs e)
        {
            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;
            c.CurrHP++;
        }

        private void btnDecCurrHp_Click(object sender, RoutedEventArgs e)
        {
            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;
            c.CurrHP--;
        }

        private void btnResetCurrHp_Click(object sender, RoutedEventArgs e)
        {
            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;
            c.CurrHP = c.MaxHP;
        }

        private void btnIncTempHp_Click(object sender, RoutedEventArgs e)
        {
            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;
            c.TempHP++;
        }

        private void btnDecTempHp_Click(object sender, RoutedEventArgs e)
        {
            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;
            c.TempHP--;
        }

        private void btnResetTempHP_Click(object sender, RoutedEventArgs e)
        {
            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;
            c.TempHP = 0;
        }

        private void btnIncOngoingDamage_Click(object sender, RoutedEventArgs e)
        {
            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;
            c.OnGoingDamage++;
        }

        private void btnDecOngoingDamage_Click(object sender, RoutedEventArgs e)
        {
            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;
            c.OnGoingDamage--;
        }

        private void btnResetOngoingDamage_Click(object sender, RoutedEventArgs e)
        {
            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;
            c.OnGoingDamage = 0;
        }

        private void btnTakeOngoing_Click(object sender, RoutedEventArgs e)
        {
            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;
            NotificationBoxWithInput notification = null;
            NotificationBoxWithInput.ShowWithNumberInput(
                "Damage",
                "How much damage?",
                c.OnGoingDamage.ToString(),
                GetType().ToString(),
                out notification,
                new NotificationBoxCommand("Okay", () => { TakeDamage(notification.NumberInputOption); }),
                new NotificationBoxCommand("Cancel", () => { }));
        }

        private void btnTakeDamage_Click(object sender, RoutedEventArgs e)
        {
            NotificationBoxWithInput notification = null;
            NotificationBoxWithInput.ShowWithNumberInput(
                "Damage",
                "How much damage?",
                "",
                GetType().ToString(),
                out notification,
                new NotificationBoxCommand("Okay", () => { TakeDamage(notification.NumberInputOption); }),
                new NotificationBoxCommand("Cancel", () => { }));
        }

        private void TakeDamage(string amount)
        {
            if (amount == "")
            {
                amount = "0";
            }
            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;
            c.TakeDamage(int.Parse(amount));
        }

        private void btnResetSurges_Click(object sender, RoutedEventArgs e)
        {
            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;
            c.CurrSurges = c.MaxSurges;
        }

        private void btnUseSurge_Click(object sender, RoutedEventArgs e)
        {
            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;

            NotificationBoxWithInput notification = null;
            NotificationBoxWithInput.ShowWithNumberInput(
                "Use Surge",
                "Using a surge will restore " + c.SurgeValue +" HP. How many HP to restore in addition?",
                "",
                GetType().ToString(),
                out notification,
                new NotificationBoxCommand("Okay", () => { UseSurge(notification.NumberInputOption); }),
                new NotificationBoxCommand("Cancel", () => { }));
        }

        private void UseSurge(string additionalHP)
        {
            if (additionalHP == "")
            {
                additionalHP = "0";
            }

            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;
            c.UseSurge(int.Parse(additionalHP));
        }

        private void btnShortRest_Click(object sender, RoutedEventArgs e)
        {
            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;
            int suggestedSurges = c.GetShortRestSuggestedSurgesToUse();

            NotificationBoxWithInput notification = null;
            NotificationBoxWithInput.ShowWithNumberInput(
                "Short Rest",
                "How many surges would you like to use during this short rest?",
                suggestedSurges.ToString(),
                GetType().ToString(),
                out notification,
                new NotificationBoxCommand("Okay", () => { ShortRest(notification.NumberInputOption); }),
                new NotificationBoxCommand("Cancel", () => { }));
        }

        private void ShortRest(string NumberOfSurges)
        {
            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;
            int surges = int.Parse(NumberOfSurges);

            c.ShortRest(surges);
        }

        private void btnExtendedRest_Click(object sender, RoutedEventArgs e)
        {
            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;
            int restHP = c.CurrSurges * c.SurgeValue > c.MissingHP ? c.MissingHP : c.CurrSurges * c.SurgeValue;

            NotificationBoxWithInput notification = null;
            NotificationBoxWithInput.ShowWithNumberInput(
                "Extended Rest",
                "Resting will add " + restHP + " HP. How many additional HP would you like to add?",
                "0",
                GetType().ToString(),
                out notification,
                new NotificationBoxCommand("Okay", () => { ExtendedRest(notification.NumberInputOption); }),
                new NotificationBoxCommand("Cancel", () => { }));
        }

        private void ExtendedRest(string AdditionalHP)
        {
            CharViewModel.Char c = (CharViewModel.Char)pivot.SelectedItem;
            int additionalHP = int.Parse(AdditionalHP);

            c.ExtendedRest(additionalHP);
        }

        private void pivot_Loaded(object sender, RoutedEventArgs e)
        {
            CharViewModel cvm = (CharViewModel)App.Current.Resources["CharViewModelDataSource"];
            if (cvm.Chars.Count == 0)
            {
                NotificationBox.Show(
                    "No Characters Found",
                    "Could not find any character. Would you like to add one now?",
                    new NotificationBoxCommand("Yes", () => { btnAddChar_Click(sender, e); }),
                    new NotificationBoxCommand("No", () => { }));
            }
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            int index = 0;
            using (var stream =
                new IsolatedStorageFileStream("activecharindex.txt", FileMode.OpenOrCreate, FileAccess.Read, IsolatedStorageFile.GetUserStoreForApplication()))
            {
                using (var reader = new StreamReader(stream))
                {
                    if (!reader.EndOfStream)
                    {
                        try
                        {
                            index = Int16.Parse(reader.ReadToEnd());
                        }
                        catch (FormatException)
                        {
                            index = 0;
                        }
                    }
                }
            }
            pivot.SelectedIndex = index;
        }

        private void PhoneApplicationPage_Unloaded(object sender, RoutedEventArgs e)
        {
            using (var stream =
                new IsolatedStorageFileStream("activecharindex.txt", FileMode.Truncate, FileAccess.Write, IsolatedStorageFile.GetUserStoreForApplication()))
            {
                using (var writer = new StreamWriter(stream))
                {
                    writer.WriteLine(pivot.SelectedIndex.ToString());
                }
            }
        }

        private void GenericTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }
    }
}