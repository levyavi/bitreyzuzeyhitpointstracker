﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.IO.IsolatedStorage;


namespace BitreyZuzeyHitPointsTracker
{
    public class CharViewModel : INotifyPropertyChanged
    {
        public CharViewModel()
        {
            Chars = new ObservableCollection<Char>();
            Chars.Add(new Char() { Name = "Avi", MaxHP = 5, Bloodied = 2, MaxSurges = 8, SurgeValue = 2 });
            //Chars.Add(new Char() { Name = "Eric", MaxHP = "7", Bloodied = "3", MaxSurges = "7", SurgeValue = "3" });
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private ObservableCollection<Char> _chars;

        public ObservableCollection<Char> Chars
        {
            get { return _chars; }
            set
            {
                if (_chars != value)
                {
                    _chars = value;
                    OnPropertyChanged("Chars");
                }
            }
        }

        public void AddChar(Char c)
        {
            Chars.Add(c);
            OnPropertyChanged("Chars");
            OnPropertyChanged("ShouldShowPivot");
            OnPropertyChanged("ShouldNotShowPivot");
        }

        public void DeleteChar(int index)
        {
            Chars.RemoveAt(index);
            OnPropertyChanged("Chars");
            OnPropertyChanged("ShouldShowPivot");
            OnPropertyChanged("ShouldNotShowPivot");
        }

        public void Save()
        {
            using (var stream =
                new IsolatedStorageFileStream("chars.txt", FileMode.Truncate, FileAccess.Write, IsolatedStorageFile.GetUserStoreForApplication()))
            {
                using (var writer = new StreamWriter(stream))
                {
                    foreach (Char c in Chars)
                    {
                        writer.WriteLine(c.ToString());
                    }
                }
            }

        }

        public void Load()
        {
            Chars.Clear();

            using (var stream =
                new IsolatedStorageFileStream("chars.txt", FileMode.OpenOrCreate, FileAccess.Read, IsolatedStorageFile.GetUserStoreForApplication()))
            {
                using (var reader = new StreamReader(stream))
                {
                    while (!reader.EndOfStream)
                    {

                        Char c = Char.Load(reader.ReadLine());
                        this.AddChar(c);
                    }
                }
            }
        }
        
        public bool ShouldShowPivot
        {
            get
            {
                return _chars.Count > 0;
            }
        }

        public bool ShouldNotShowPivot
        {
            get
            {
                return _chars.Count == 0;
            }
        }

        public class Char : INotifyPropertyChanged
        {
            private string _name;
            private int _maxHP;
            private int _bloodied;
            private int _maxSurges;
            private int _surgeValue;
            private int _currHP;
            private int _tempHP;
            private int _onGoingDamage;
            private int _currSurges;

            public string Name
            {
                get { return _name; }
                set
                {
                    if (_name != value)
                    {
                        _name = value;
                        OnPropertyChanged("Name");
                    }
                }
            }

            public int MaxHP
            {
                get { return _maxHP; }
                set
                {
                    if (_maxHP != value)
                    {
                        _maxHP = value;
                        OnPropertyChanged("MaxHP");
                    }
                }
            }

            public int Bloodied
            {
                get { return _bloodied; }
                set
                {
                    if (_bloodied != value)
                    {
                        _bloodied = value;
                        OnPropertyChanged("Bloodied");
                    }
                }
            }

            public int MaxSurges
            {
                get { return _maxSurges; }
                set
                {
                    if (_maxSurges != value)
                    {
                        _maxSurges = value;
                        OnPropertyChanged("MaxSurges");
                    }
                }
            }

            public int SurgeValue
            {
                get { return _surgeValue; }
                set
                {
                    if (_surgeValue != value)
                    {
                        _surgeValue = value;
                        OnPropertyChanged("SurgeValue");
                    }
                }
            }

            public int CurrHP
            {
                get { return _currHP; }
                set
                {
                    if (_currHP != value)
                    {
                        _currHP = value > MaxHP ? MaxHP : value;
                        OnPropertyChanged("CurrHP");
                    }
                }
            }

            public int TempHP
            {
                get { return _tempHP; }
                set
                {
                    if (_tempHP != value)
                    {

                        _tempHP = value < 0 ? 0 : value;
                        OnPropertyChanged("TempHP");
                    }
                }
            }

            public int OnGoingDamage
            {
                get { return _onGoingDamage; }
                set
                {
                    if (_onGoingDamage != value)
                    {
                        _onGoingDamage = value < 0 ? 0 : value;
                        OnPropertyChanged("OnGoingDamage");
                    }
                }
            }

            public int CurrSurges
            {
                get { return _currSurges; }
                set
                {
                    if (_currSurges != value)
                    {
                        _currSurges = value;
                        OnPropertyChanged("CurrSurges");
                    }
                }
            }

            public int MissingHP
            {
                get 
                {
                    return (CurrHP < 0 ? MaxHP : MaxHP - CurrHP);
                }
            }

            public int GetShortRestSuggestedSurgesToUse()
            {
                if (SurgeValue == 0)
                {
                    return 0;
                }

                int result = 0;
                int missingSurges = MissingHP / SurgeValue;
                result = missingSurges < MaxSurges ? missingSurges : MaxSurges;

                return result;
            }

            public void TakeDamage(int amount)
            {
                int dmgLeft = amount;
                int fromTemp = 0;
                fromTemp = TempHP > amount ? amount : TempHP;
                TempHP -= fromTemp;
                dmgLeft -= fromTemp;
                CurrHP -= dmgLeft;
            }

            public void UseSurge(int additionalHP)
            {
                if (CurrSurges < 1)
                {
                    return;
                }

                CurrSurges--;
                if (CurrHP < 0)
                {
                    CurrHP = 0;
                }
                CurrHP += SurgeValue;
                CurrHP += additionalHP;
            }

            public void ShortRest(int numberOfSurges)
            {
                if (numberOfSurges == 0)
                {
                    return;
                }
                CurrHP = CurrHP < 0 ? 0 : CurrHP;
                int actualSurges = numberOfSurges < MaxSurges ? numberOfSurges : MaxSurges;
                CurrSurges -= actualSurges;
                CurrHP += actualSurges * SurgeValue;
            }

            public void ExtendedRest(int additionalHP)
            {
                CurrHP += SurgeValue * CurrSurges;
                CurrHP += additionalHP;
                TempHP = 0;
                CurrSurges = MaxSurges;
                OnGoingDamage = 0;
            }

            public void ResetChar()
            {
                CurrHP = MaxHP;
                TempHP = 0;
                CurrSurges = MaxSurges;
                OnGoingDamage = 0;
            }

            public event PropertyChangedEventHandler PropertyChanged;

            public void OnPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }

            public override string ToString()
            {
                return string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}",
                    Name, MaxHP, Bloodied, MaxSurges, SurgeValue, CurrHP, TempHP, OnGoingDamage, CurrSurges);
            }

            public static Char Load(string s)
            {
                string[] f = s.Split(";".ToCharArray());
                Char n = new Char() { Name = f[0], MaxHP = int.Parse(f[1]), Bloodied = int.Parse(f[2]), MaxSurges = int.Parse(f[3]), 
                    SurgeValue = int.Parse(f[4]), CurrHP = int.Parse(f[5]), TempHP = int.Parse(f[6]), OnGoingDamage = int.Parse(f[7]), CurrSurges = int.Parse(f[8]) };
                return n;
            }
        }
    }
}