﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using Microsoft.Phone.Controls;
using System.ComponentModel;

namespace BitreyZuzey.Phone.Controls
{
    public abstract class BitreyZuzeyNotificationBox : Control, ICloseable
    {
        public abstract void Close();

        #region Fields

        protected static Popup _popup;
        protected static bool _appBarVisibility;

        #endregion

        #region Privates

        protected static PhoneApplicationPage CurrentPage
        {
            get
            {
                var rootFrame = Application.Current.RootVisual as PhoneApplicationFrame;
                var currentPage = rootFrame.Content as PhoneApplicationPage;
                return currentPage;
            }
        }

        protected static void HandleBackKeyAndAppBar()
        {
            CurrentPage.BackKeyPress += parentPage_BackKeyPress;
            if (CurrentPage.ApplicationBar != null)
            {
                _appBarVisibility = CurrentPage.ApplicationBar.IsVisible;
                CurrentPage.ApplicationBar.IsVisible = false;
            }
        }

        private static void parentPage_BackKeyPress(object sender, CancelEventArgs e)
        {
            CurrentPage.BackKeyPress -= parentPage_BackKeyPress;
            ClosePopup();

            e.Cancel = true;
        }

        protected static void ClosePopup()
        {
            if (_popup != null)
            {
                _popup.IsOpen = false;
                _popup = null;
                CurrentPage.BackKeyPress -= parentPage_BackKeyPress;
            }

            if (CurrentPage.ApplicationBar != null)
            {
                CurrentPage.ApplicationBar.IsVisible = _appBarVisibility;
            }
        }

        #endregion
    }
}
