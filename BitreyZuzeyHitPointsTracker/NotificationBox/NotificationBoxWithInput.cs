﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Windows.Controls.Primitives;

namespace BitreyZuzey.Phone.Controls
{
    /// <summary>
    /// Represents a message-box like control for displaying a message box
    /// with custom actions an option to persist 'show message again' state.
    /// </summary>
    public class NotificationBoxWithInput : BitreyZuzeyNotificationBox
    {
        #region Properties

        #region CommandsSource Property

        /// <summary>
        /// Gets or sets a collection of <see cref="NotificationBoxCommand"/>.
        /// </summary>
        public IEnumerable<NotificationBoxCommand> CommandsSource
        {
            get { return (IEnumerable<NotificationBoxCommand>)GetValue(CommandsSourceProperty); }
            set { SetValue(CommandsSourceProperty, value); }
        }

        /// <value>Identifies the CommandsSource dependency property</value>
        public static readonly DependencyProperty CommandsSourceProperty =
            DependencyProperty.Register(
            "CommandsSource",
            typeof(IEnumerable<NotificationBoxCommand>),
            typeof(NotificationBoxWithInput),
              new PropertyMetadata(default(IEnumerable<NotificationBoxCommand>), CommandsSourceChanged));

        private static void CommandsSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var newCommands = e.NewValue as IEnumerable<NotificationBoxCommand>;
            if (newCommands != null)
            {
                var box = d as NotificationBoxWithInput;
                foreach (var newCommand in newCommands)
                {
                    newCommand.Owner = box;
                }
            }
        }

        #endregion

        #region Title Property

        /// <summary>
        /// Gets or sets the title text to display.
        /// </summary>
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        /// <value>Identifies the Title dependency property</value>
        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register(
            "Title",
            typeof(string),
            typeof(NotificationBoxWithInput),
              new PropertyMetadata(default(string)));

        #endregion

        #region Message Property

        /// <summary>
        /// Gets or sets the message text to display.
        /// </summary>
        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        /// <value>Identifies the Message dependency property</value>
        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register(
            "Message",
            typeof(string),
            typeof(NotificationBoxWithInput),
              new PropertyMetadata(default(string)));

        #endregion

        #region TextInputOption Property

        /// <summary>
        /// Gets or sets a value for indicating if this message is to be shown again.
        /// </summary>
        public string TextInputOption
        {
            get { return (string)GetValue(TextInputOptionProperty); }
            set { SetValue(TextInputOptionProperty, value); }
        }

        /// <value>Identifies the TextInputOption dependency property</value>
        public static readonly DependencyProperty TextInputOptionProperty =
            DependencyProperty.Register(
            "TextInputOption",
            typeof(string),
            typeof(NotificationBoxWithInput),
              new PropertyMetadata(default(string)));

        #endregion

        #region TextInputVisibility Property

        /// <summary>
        /// Gets or sets a value indicating if the text input message is visible or not.
        /// </summary>
        public Visibility TextInputVisibility
        {
            get { return (Visibility)GetValue(TextInputVisibilityProperty); }
            set { SetValue(TextInputVisibilityProperty, value); }
        }

        /// <value>Identifies the TextInputVisibility dependency property</value>
        public static readonly DependencyProperty TextInputVisibilityProperty =
            DependencyProperty.Register(
            "TextInputVisibility",
            typeof(Visibility),
            typeof(NotificationBoxWithInput),
              new PropertyMetadata(default(Visibility)));

        #endregion

        #region NumberInputOption Property

        /// <summary>
        /// Gets or sets a value for indicating if this message is to be shown again.
        /// </summary>
        public string NumberInputOption
        {
            get { return (string)GetValue(NumberInputOptionProperty); }
            set { SetValue(NumberInputOptionProperty, value); }
        }

        /// <value>Identifies the TextInputOption dependency property</value>
        public static readonly DependencyProperty NumberInputOptionProperty =
            DependencyProperty.Register(
            "NumberInputOption",
            typeof(string),
            typeof(NotificationBoxWithInput),
              new PropertyMetadata(default(string)));

        #endregion

        #region NumberInputVisibility Property

        /// <summary>
        /// Gets or sets a value indicating if the text input message is visible or not.
        /// </summary>
        public Visibility NumberInputVisibility
        {
            get { return (Visibility)GetValue(NumberInputVisibilityProperty); }
            set { SetValue(NumberInputVisibilityProperty, value); }
        }

        /// <value>Identifies the TextInputVisibility dependency property</value>
        public static readonly DependencyProperty NumberInputVisibilityProperty =
            DependencyProperty.Register(
            "NumberInputVisibility",
            typeof(Visibility),
            typeof(NotificationBoxWithInput),
              new PropertyMetadata(default(Visibility)));

        #endregion
        
        private string UniqueKey { get; set; }

        #endregion

        #region Ctor

        private NotificationBoxWithInput()
        {
            DefaultStyleKey = typeof(NotificationBoxWithInput);
            
        }

        #endregion        

        #region Utilities


        /// <summary>
        /// Displays a notification box with title, message and custom actions.
        /// In addition a message asking if this message should be shown again next time.
        /// </summary>
        /// <param name="title">The title of this message.</param>
        /// <param name="message">The message body text.</param>        
        /// <param name="textInputText"></param>
        /// <param name="uniqueKey">Unique key representing a specific message identity.</param>
        /// <param name="notificationBox"></param>
        /// <param name="commands">A collection of actions.</param>
        public static void ShowWithTextInput(string title, string message, string textInputText, string uniqueKey, out NotificationBoxWithInput notificationBox, params NotificationBoxCommand[] commands)
        {
            if (_popup != null)
            {
                throw new InvalidOperationException("Message is already shown.");
            }

            HandleBackKeyAndAppBar();
            _popup = new Popup
            {
                IsOpen = true,
                Child = new NotificationBoxWithInput
                {
                    Width = Application.Current.RootVisual.RenderSize.Width,
                    Height = Application.Current.RootVisual.RenderSize.Height,
                    UniqueKey = uniqueKey.ToString(),
                    Title = title,
                    Message = message,
                    CommandsSource = commands,
                    TextInputOption = textInputText,
                    TextInputVisibility = Visibility.Visible,
                    NumberInputVisibility = Visibility.Collapsed
                }
            };
            notificationBox = (NotificationBoxWithInput)_popup.Child;
        }

        /// <summary>
        /// Displays a notification box with title, message and custom actions.
        /// In addition a message asking if this message should be shown again next time.
        /// </summary>
        /// <param name="title">The title of this message.</param>
        /// <param name="message">The message body text.</param>        
        /// <param name="suppression">Callback for indicating whether message suppressed or not..</param>
        /// <param name="uniqueKey">Unique key representing a specific message identity.</param>
        /// <param name="commands">A collection of actions.</param>
        public static void ShowWithNumberInput(string title, string message, string numberInputOption, string uniqueKey, out NotificationBoxWithInput notificationBox, params NotificationBoxCommand[] commands)
        {
            if (_popup != null)
            {
                throw new InvalidOperationException("Message is already shown.");
            }

            HandleBackKeyAndAppBar();
            _popup = new Popup
            {
                IsOpen = true,
                Child = new NotificationBoxWithInput
                {
                    Width = Application.Current.RootVisual.RenderSize.Width,
                    Height = Application.Current.RootVisual.RenderSize.Height,
                    UniqueKey = uniqueKey.ToString(),
                    Title = title,
                    Message = message,
                    CommandsSource = commands,
                    NumberInputOption = numberInputOption,
                    TextInputVisibility = Visibility.Collapsed,
                    NumberInputVisibility = Visibility.Visible
                }
            };
            notificationBox = (NotificationBoxWithInput)_popup.Child;
        }
        

        #endregion

        public override void Close()
        {
            ClosePopup();
        }

    }
}
